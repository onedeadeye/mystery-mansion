using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerCamera : MonoBehaviour
{

    [SerializeField]
    private float mouseSensitivity = 100f;

    [SerializeField]
    public Transform playerBody;

    private Animator animator;

    private float xRotation = 0f;

    public static PlayerCamera playerCamera;

    // Start is called before the first frame update
    void Start()
    {
        playerCamera = this;
        animator = GetComponent<Animator>();
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity;

        playerBody.Rotate(Vector3.up * mouseX);

        xRotation -= mouseY;

        xRotation = Mathf.Clamp(xRotation, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
    }

    public void FadeToFromBlack()
    {
        animator.Play("FadeToFromBlack", 0, 0f);
    }
}
