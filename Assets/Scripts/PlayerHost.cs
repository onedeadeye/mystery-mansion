using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerHost : MonoBehaviour
{

    public static PlayerHost player;

    [SerializeField]
    private bool atInteractable = false;
    [SerializeField]
    private MapInteractable lastInteractable;

    private CharacterController controller;

    [SerializeField]
    private Transform groundCheck;
    [SerializeField]
    private float groundDistance = 0.4f;
    [SerializeField]
    private LayerMask groundMask;

    [SerializeField]
    private float speed = 12f;
    [SerializeField]
    private float gravity = -9.81f;
    private float landForce = -2f;

    Vector3 velocity;
    bool isGrounded;
    bool inCinematic = false;
    GameObject cinematicBind;

    [SerializeField]
    private ArrayList heldKeys = new ArrayList();

    // Start is called before the first frame update
    void Start()
    {
        player = this;
        controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        velocity.y += gravity * Time.deltaTime;

        if (isGrounded && velocity.y < 0)
        {
            velocity.y = landForce;
        }

        controller.Move(velocity * Time.deltaTime);

        if (!inCinematic)
        {
            controller.Move(move * speed * Time.deltaTime);

            if (atInteractable)
            {
                if (Input.GetButtonDown("Interact"))
                {
                    lastInteractable.Interact();
                }
            }
        }
        else
        {
            PlayerCamera.playerCamera.gameObject.transform.position = cinematicBind.transform.position;
        }

    }

    public void SetAtInteractable(bool isAtInteractable)
    {
        atInteractable = isAtInteractable;
    }

    public void SetLastInteractable(MapInteractable interactable)
    {
        lastInteractable = interactable;
    }

    public void AddKey(string key)
    {
        heldKeys.Add(key);
    }

    public bool TestKey(string key)
    {
        return heldKeys.Contains(key);
    }

    public void Teleport(Vector3 position)
    {
        controller.enabled = false;
        transform.position = position;
        controller.enabled = true;
    }

    public void BindCinematicCamera(GameObject bind)
    {
        cinematicBind = bind;
        PlayerCamera.playerCamera.gameObject.transform.rotation = cinematicBind.transform.rotation;
        inCinematic = true;
    }

    public void UnbindCinematicCamera()
    {
        cinematicBind = null;
        inCinematic = false;
        PlayerCamera.playerCamera.transform.localPosition = new Vector3(0, 0.5f, 0);
        PlayerCamera.playerCamera.transform.localEulerAngles = Vector3.zero;
    }
}
