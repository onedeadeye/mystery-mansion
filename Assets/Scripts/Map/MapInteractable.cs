using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public abstract class MapInteractable : MonoBehaviour
{

    // The maximum angle between the MapInteractable and the direction the player is looking
    // to still allow an interaction
    [SerializeField]
    private float maximumOffsetLookAngle = 35f;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public abstract void Interact();

    public void MidInteractFade()
    {
        PlayerCamera.playerCamera.FadeToFromBlack();
    }

    public void EndInteract()
    {
        PlayerHost.player.UnbindCinematicCamera();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHost.player.SetLastInteractable(this);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (Vector3.Angle(transform.forward, other.transform.forward) <= maximumOffsetLookAngle)
            {
                PlayerHost.player.SetAtInteractable(true);
            }
            else
            {
                PlayerHost.player.SetAtInteractable(false);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHost.player.SetAtInteractable(false);
        }
    }
}
