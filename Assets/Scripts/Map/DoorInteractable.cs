using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class DoorInteractable : MapInteractable
{
    [SerializeField]
    private string doorKey = "";

    [SerializeField]
    private GameObject transitionCameraPoint;

    [SerializeField]
    private GameObject exitPlayerPoint;

    [SerializeField]
    private DoorInteractable linkedDoor;

    private bool doorLocked = false;

    private Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        if (doorKey != string.Empty)
        {
            doorLocked = true;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void Interact()
    {
        if (!doorLocked)
        {
            animator.Play("DoorOpen", 0, 0f);
            PlayerHost.player.BindCinematicCamera(transitionCameraPoint);
        }
        else
        {
            if (PlayerHost.player.TestKey(doorKey))
            {
                animator.Play("DoorUnlock", 0, 0f);
                PlayerHost.player.BindCinematicCamera(transitionCameraPoint);
                doorLocked = false;
            }
            else
            {
                animator.Play("DoorWiggle", 0, 0f);
                PlayerHost.player.BindCinematicCamera(transitionCameraPoint);
            }
        }

    }

    public void FinishInteract()
    {
        PlayerHost.player.Teleport(linkedDoor.getExitPlayerPoint());
        EndInteract();
    }

    public Vector3 getExitPlayerPoint()
    {
        return exitPlayerPoint.transform.position;
    }
}
